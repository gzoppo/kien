import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CategoriesComponent } from './components/categories/categories.component';
import { ProductsComponent } from './components/products/products.component';

@NgModule({
  imports: [RouterModule.forRoot([
    {
      path: '',
      component: CategoriesComponent
    },
    {
      path: 'categoria/:id',
      component: ProductsComponent
    }
  ], {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
