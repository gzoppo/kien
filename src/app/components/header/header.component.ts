import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalUsersComponent } from './modal-users/modal-users.component';
import { OrderService } from '../../services/order.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userLogged: boolean;

  constructor(
    private dialog: MatDialog,
    private _orderService: OrderService,
    private _userService: UserService
  ) { }

  ngOnInit() {
    this._userService.userSubject.subscribe((response) => {
      if (response) {
        this.userLogged = true;
      }
    })
  }

  openModalUsers() {
    this.dialog.open(ModalUsersComponent, {
      width: '350px'
    })
  }

  openOrder() { this._orderService.orderOpened.next(!this._orderService.orderOpened.getValue()); }

  closeOrder() { this._orderService.orderOpened.next(false); }

  logoutXD() { window.location.reload() };
}
