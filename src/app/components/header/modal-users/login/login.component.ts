import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../modal.styles.scss']
})
export class LoginComponent implements OnInit {

  @Output() toRegister: EventEmitter<string>;
  loginForm: FormGroup;
  sendingForm: boolean;

  constructor(
    private snackbar: MatSnackBar,
    private _userService: UserService
  ) {
    this.toRegister = new EventEmitter<string>();
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      "user": new FormControl('', [Validators.required]),
      "password": new FormControl('', [Validators.required]),
    })
  }

  changeModal() { this.toRegister.emit('register'); }

  submitForm() {
    if (this.loginForm.valid) {
      this.sendingForm = true;
      this._userService.loginUser(this.loginForm.value).then(
        (response) => {
          if (response) {
            this.toRegister.emit('close');
          }
        },
        (error) => {
          this.alertSnackbar(error.error.error);
          this.sendingForm = false;
        }
      )
    }
  }

  alertSnackbar(message: string) { this.snackbar.open(message, '', { duration: 5000, }); }

}