import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-users',
  templateUrl: './modal-users.component.html'
})
export class ModalUsersComponent {

  status: string;

  constructor(
    private dialogRef: MatDialogRef<ModalUsersComponent>
  ) {
    this.status = 'login';
  }

  changeModal(status: string) {
    if (status == 'close') {
      this.dialogRef.close();
    } else {
      this.status = status;
    }
  }

}
