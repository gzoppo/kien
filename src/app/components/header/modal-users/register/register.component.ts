import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../modal.styles.scss']
})
export class RegisterComponent implements OnInit {

  @Output() toLogin: EventEmitter<string>;
  registerForm: FormGroup;
  sendingForm: boolean;
  registered: boolean;

  constructor(
    private snackbar: MatSnackBar,
    private _userService: UserService
  ) {
    this.toLogin = new EventEmitter<string>();
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      "nombre": new FormControl('', [Validators.required]),
      "user": new FormControl('', [Validators.required]),
      "correo": new FormControl('', [Validators.required, Validators.email]),
      "telefono": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
      "password": new FormControl('', [Validators.required])
    });
  }

  changeModal() { this.toLogin.emit('login'); }

  submitForm() {
    if (this.registerForm.valid) {
      this.sendingForm = true;
      let user = this.registerForm.value;
      user.tipo = 1;
      this._userService.registerUser(user).toPromise().then(
        (response) => {
          this.registered = true;
        },
        (error) => {
          this.alertSnackbar(error.error.error);
          this.sendingForm = false;
        }
      )
    }
  }
  
  alertSnackbar(message: string) { this.snackbar.open(message, '', { duration: 5000, }); }

}
