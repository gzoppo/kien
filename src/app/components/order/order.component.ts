import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { OrderService } from '../../services/order.service';
import { ThanksComponent } from './thanks/thanks.component';
import { UserService } from 'app/services/user.service';
import { ModalUsersComponent } from '../header/modal-users/modal-users.component';

@Component({selector: 'app-order',templateUrl: './order.component.html',styleUrls: ['./order.component.scss']})

export class OrderComponent implements OnInit {
  products: Array<any>;
  total: number;
  shipping: string;
  shippingForm: FormGroup;
  mapUbication;
  hasNavigator: boolean;
  disabledUbication: boolean;
  sendingForm: boolean;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private _orderService: OrderService,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this._orderService.orderSubject.subscribe((response) => {
      this.products = response;
      this.getTotal();
    });
    this.shippingForm = new FormGroup({
      "direccion": new FormControl('', [Validators.required, Validators.maxLength(100)]),
      "descripcion": new FormControl('', [Validators.maxLength(1000)])
    });
  }

  setUbication() {
    if (!this.disabledUbication) {
      if (navigator) {
        navigator.geolocation.getCurrentPosition(
          (pos) => {
            this.mapUbication = {
              lat: +pos.coords.latitude,
              lng: +pos.coords.longitude
            };
            this.hasNavigator = true;
          },
          (err) => {
            this.snackbar.open('Su navegador no soporte o no tiene habilitada la geolocalizacion para este sitio.');
            this.disabledUbication = true;
          }
        );
      } else {
        this.snackbar.open('Su navegador no soporte o no tiene habilitada la geolocalizacion para este sitio.');
        this.disabledUbication = true;
      }
    }
  }

  getTotal() { 
    let total = 0;
    for (let i = 0; i < this.products.length; i++) {
      const e = this.products[i];
      total += e.costo * e.cantidad;
    }
    this.total = Number(total.toFixed(2));
  }

  submitOrder() {
    const user = this._userService.userSubject.getValue();
    if (user) {
      if (this.shipping) {
        let products = this.products.map((e) => {
          e.product_id = e.id;
          return e;
        });
        this.sendingForm = true;
        let order = {
          total: this.total.toString(),
          productos: JSON.stringify(products),
          lat: 0,
          lng: 0,
          direccion: "",
          descripcion: "",
          usuario_id: parseInt(user.user.id),
          token: user.token,
        }
        switch (this.shipping) {
          case "1":
            if (this.shippingForm.valid) {
              order.direccion = this.shippingForm.get('direccion').value;
              order.descripcion = this.shippingForm.get('descripcion').value;
              this.sendOrder(order);
            } else {
              this.alertSnackbar('No selecciono los datos de envió.');
              this.sendingForm = false;
            }
            break;
    
          case "2":
            if (this.mapUbication) {
              order.lat = this.mapUbication.lat;
              order.lng = this.mapUbication.lng;
              this.sendOrder(order);
            } else {
              this.alertSnackbar('No selecciono los datos de envió.');
              this.sendingForm = false;
            }
            break;
        
          default:
            break;
        }
      } else {
        this.alertSnackbar('No selecciono los datos de envió.');
      }
    } else {
      this.dialog.open(ModalUsersComponent, {
        width: '350px'
      })
    }
  }

  sendOrder(order) {
    this._orderService.submitOrder(order).toPromise().then(
      (response) => {
        this._orderService.orderOpened.next(false);
        this.dialog.open(ThanksComponent, {
          width: '350px'
        });
      }
    )
  }

  alertSnackbar(message: string) { this.snackbar.open(message, '', { duration: 5000, }); }

}