import { Component, OnInit, Input } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-card-order',
  templateUrl: './product-card-order.component.html',
  styleUrls: ['./product-card-order.component.scss']
})
export class ProductCardOrderComponent implements OnInit {

  @Input() product: any;
  quantityControl: FormControl;

  constructor(
    private _orderService: OrderService
  ) { }

  ngOnInit() {
    this.quantityControl = new FormControl(this.product.cantidad ? this.product.cantidad : 1, [Validators.min(1)]);
    this.product.cantidad = this.product.cantidad ? this.product.cantidad : 1;
    this.quantityControl.valueChanges.subscribe((value) => {
      if (value || value == 0) {
        if (value < 0) {
          value = 1;
        }
        this.quantityControl.setValue(value.toString().replace(/(\.\d{2})\d+/g, '$1'), { emitEvent: false });
        this._orderService.changeQuantity(this.product, this.quantityControl.value);
      }
    })
  }

  removeProduct(product) { this._orderService.removeProduct(product); }

}
