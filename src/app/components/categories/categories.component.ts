import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Array<any>;

  constructor(
    private _productsService: ProductsService
  ) { }

  ngOnInit() {
    this._productsService.categories.subscribe((response) => {
      this.categories = response;
    })
  }

}
