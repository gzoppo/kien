import { Component, OnInit, Input } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { OrderService } from '../../../services/order.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product: any;
  inOrder: boolean;

  constructor(
    private snackbar: MatSnackBar,
    private _orderService: OrderService
  ) { }

  ngOnInit() {
    this.inOrder = this._orderService.hasProduct(this.product);
  }

  onChangeCheckbox(event: MatCheckboxChange) {
    if (event.checked) {
      this.alertSnackbar('Se agregó el producto corretamente a su pedido.');
      this._orderService.addProduct(this.product);
    } else {
      this.alertSnackbar('Se quitò el producto corretamente a su pedido.');
      this._orderService.removeProduct(this.product);
    }
  }

  alertSnackbar(message: string) { this.snackbar.open(message, '', { duration: 5000, }); }

}
