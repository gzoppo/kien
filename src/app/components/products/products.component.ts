import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  title: string;
  products: Array<any>;
  categories: Array<any>;
  categoryControl: FormControl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _productsService: ProductsService
  ) {
    this.title = 'Productos';
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      const categoryId = paramMap.get('id');
      this.categoryControl = new FormControl(categoryId);
      this.subscribeCategory();
      this._productsService.categories.subscribe((response) => {
        if (response) {
          const i = response.map((e) => { return e.id }).indexOf(categoryId);
          if (i != -1) {
            this.products = response[i].productos;
            this.categories = response.map((e) => {
              return {
                id: e.id,
                name: e.nombre
              }
            });
          } else {
            this.router.navigate(['/']);
          }
        }
      })
    })
  }
  
  subscribeCategory() {
    this.categoryControl.valueChanges.subscribe((value) => {
      this.router.navigate(['/categoria', value]);
    })
  }

}