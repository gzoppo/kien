import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ModalUsersComponent } from './components/header/modal-users/modal-users.component';
import { TitleSectionComponent } from './components/common/title-section/title-section.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductCardComponent } from './components/products/product-card/product-card.component';
import { OrderComponent } from './components/order/order.component';
import { LoginComponent } from './components/header/modal-users/login/login.component';
import { RegisterComponent } from './components/header/modal-users/register/register.component';
import { ProductsService } from './services/products.service';
import { OrderService } from './services/order.service';
import { UserService } from './services/user.service';
import { ProductCardOrderComponent } from './components/order/product-card-order/product-card-order.component';
import { AgmCoreModule } from '@agm/core';
import { ThanksComponent } from './components/order/thanks/thanks.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CategoriesComponent,
    ProductsComponent,
    OrderComponent,
    TitleSectionComponent,
    ProductCardComponent,
    ModalUsersComponent,
    LoginComponent,
    RegisterComponent,
    ProductCardOrderComponent,
    ThanksComponent
  ],
  entryComponents: [
    ModalUsersComponent,
    ThanksComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCbW0NXvzhDpASP76XkzwH4aZrQ7zJD1nY'
    })
  ],
  providers: [
    ProductsService,
    OrderService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
