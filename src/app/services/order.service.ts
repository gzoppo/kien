import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class OrderService {

    urlOrder: string;

    orderSubject: BehaviorSubject<any[]>;
    order: Array<any>;
    orderOpened: BehaviorSubject<boolean>;

    constructor(
        private _http: HttpClient
    ) {
        this.order = [];
        const LS = localStorage.getItem('okien');
        if (LS) { this.order = JSON.parse(LS); }
        this.orderSubject = new BehaviorSubject<any[]>(this.order);
        this.orderOpened = new BehaviorSubject<boolean>(false);
        this.urlOrder = `${environment.api}/pedidos`;
    }

    addProduct(product) { 
        const i = this.order.map((e) => { return e.id }).indexOf(product.id);
        if (i == -1) {
            let p = product;
            p.cantidad = "1.00";
            this.order.push(p);
            localStorage.setItem('okien', JSON.stringify(this.order));
            this.orderSubject.next(this.order);
        }
    }

    changeQuantity(product, quantity: string) {
        const i = this.order.map((e) => { return e.id }).indexOf(product.id);
        if (i != -1) {
            this.order[i].cantidad = quantity;
            localStorage.setItem('okien', JSON.stringify(this.order));
            this.orderSubject.next(this.order);
        }
    }

    removeProduct(product) {
        const i = this.order.map((e) => { return e.id }).indexOf(product.id);
        if (i != -1) {
            this.order.splice(i, 1);
            localStorage.setItem('okien', JSON.stringify(this.order));
            this.orderSubject.next(this.order);
        }
    }

    hasProduct(product) {
        const i = this.order.map((e) => { return e.id }).indexOf(product.id);
        return i == -1 ? false : true;
    }

    removeOrder() {
        localStorage.removeItem('okien');
        this.order = [];
        this.orderSubject.next(this.order);
    }

    submitOrder(order) { return this._http.post(this.urlOrder, order); }

}