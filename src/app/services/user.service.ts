import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService {

    urlRegister: string;
    urlLogin: string;
    userSubject: BehaviorSubject<any>;

    constructor(
        private _http: HttpClient
    ) {
        this.urlRegister = `${environment.api}/clientes`;
        this.urlLogin = `${environment.api}/login/app`;
        this.userSubject = new BehaviorSubject<any>(null);
    }

    registerUser(user) { return this._http.post(this.urlRegister, user); }

    loginUser(credentials) { 
        return this._http.post(this.urlLogin, credentials ).toPromise().then(
            (response: any) => {
                this.userSubject.next(response);
                return true;
            }
        );
    }

}