import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ProductsService {

    urlCategorias: string;
    urlProducts: string;

    categories: BehaviorSubject<any[]>;

    constructor(
        private _http: HttpClient
    ) {
        this.categories = new BehaviorSubject<any[]>(null);
        this.urlCategorias = `${environment.api}/categorias`;
        this.urlProducts = `${environment.api}/productos`;
    }

    getProductsByCategories() { 
        return this._http.get(`${this.urlCategorias}/productos`).toPromise().then(
            (response: any) => {
                if (response.status == "ok") {
                    this.categories.next(response.categorias);
                }
            }
        );
    }

}