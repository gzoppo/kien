import { Component, ViewChild, OnInit } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { ProductsService } from './services/products.service';
import { OrderService } from './services/order.service';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('drawer', { static: true }) drawer: MatDrawer;
  @ViewChild('scrolleable', { static: true }) scroll: any;
  drawerOpened: boolean;

  constructor(
    private router: Router,
    private _productsService: ProductsService,
    private _orderService: OrderService
  ) {
    this._productsService.getProductsByCategories();
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
          setTimeout(() => {
              this.scroll.nativeElement.scrollTo(0, 0);
          }, 100)
      }
    });
    this._orderService.orderOpened.subscribe((response) => {
      if (response) {
          this.drawer.open();
      } else {
          this.drawer.close();
      }
    });
  }

  detectChangeDrawer(event) { this.drawerOpened = event; }
  
}